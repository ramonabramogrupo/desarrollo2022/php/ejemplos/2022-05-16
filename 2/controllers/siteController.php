<?php
   
    function actionIndex(){
        render("index",[]);
    }
    
    function actionQuienes(){
        $componentes=[
            "alumno1","alumno2","alumno3","Jose","ana"
        ];
        
        render("equipo",[
            "equipo" => $componentes
        ]);
    }
    
    function actionDonde(){
        render("mapa",[]);
    }
    
    function actionConocenos(){
        global $urlBase;
        
        render("conocer",[
            "titulo" => "Empresa de formacion actual",
            "descripcion" => "descripcion de la empresa",
            "foto" =>"../imgs/empresa.jpg" // no se muestra todavia
        ]);
    }
    
    function actionContacto(){
        
        // es la primera vez que cargo la vist
        // el formulario
        if(!isset($_GET["boton"])){
            render("formulario",[]);
        }else{
            // cuando he pulsado el boton
            render("mensaje",[]);
        }
        
    }

