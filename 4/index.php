<?php
require "vendor/aplicacion.php";
// son los datos a utilizar
$datos = [
    [
        "id" => 1,
        "titulo" => "Ordenador",
        "foto" => "1.jpg"
    ],
    [
        "id" => 2,
        "titulo" => "Raton",
        "foto" => "2.jpg"
    ]
];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <ul>
            <li>
                <?= enlace("index", "inicio") ?>
            </li>
            <li>
                <a href="./listar">Listar</a>
            </li>
            <li>
                <a href="./todo?id=19">Listar Todo</a>
            </li>
            <li>
                <a href="./mensaje">Mostrar mensaje</a>
            </li>

        </ul>
        <?php
        // tengo una variable accion que tiene el
        // nombre de la accion  a ejecutar
        // accion="actionIndex"

        $accion();
        ?>
    </body>
</html>
