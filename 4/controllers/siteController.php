<?php

function actionIndex() {
    render(
            "inicio", //nombre de la vista
            [
                "mensaje" => "Ejemplo de aplicacion con datos en MVC"
            ] // parametros que debe mostrar la vista
    );
}

function actionListar() {
    global $datos; // tengo disponibles los datos
    // en la funcion
    // quito el campo foto de todos los registros
    foreach ($datos as $indice => $registro) {
        unset($datos[$indice]["foto"]);
    }

    // le mando a la vista todos los datos
    // sin el campo foto
    render("mostrar", [
        "objetos" => $datos,
        "campos" => ["Referencia", "Titulo"],
        "acciones" => true,
    ]);
}

function actionTodo() {
    global $datos;

    foreach ($datos as $indice => $registro) {

        // "../imgs/1.jpg"
        $ruta = $datos[$indice]["foto"];
        // añado la etiqueta img a la ruta
        // <img src="../imgs/1.jpg">
        $datos[$indice]["foto"] = imagen($datos[$indice]["foto"]);
    }

    // le mando a la vista todos los datos
    render("mostrar", [
        "objetos" => $datos,
        "campos" => [
            "Referencia",
            "Descripcion",
            "Imagen del producto"
        ],
        "acciones" => false,
    ]);
}

function actionMensaje() {
    render("hola", [
        "titulo" => "Bienvenidos",
        "texto" => "hola clase"
    ]);
}
